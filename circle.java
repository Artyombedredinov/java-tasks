package ru.unn.classes_and_objects;

public class circle extends figure {
    private double radius;

    public circle(int X,int Y,double Radius){
        super(X, Y);
        radius=Radius;
    }

    @Override
    public void square(){
        System.out.println();
        System.out.print("Круг ");
        System.out.printf("%.4f", radius * 3.1415);
    }
}