package ru.unn.arrays1;

import java.util.Arrays;

public class Arrays1 {
    public static void main(String[] args) {
        float [] mas = {7, 2, 25, 10, 31, 4, 1, 17};
        for (int i = 0; i < mas.length; i++) {
            mas[i] += mas[i] / 10;
        }
        System.out.println("Элементы массива увеличены на 10%: " + Arrays.toString(mas));


        boolean isSorted = false;
        float buf;
        while(!isSorted) {
            isSorted = true;
            for (int i = 0; i < mas.length-1; i++) {
                if(mas[i] < mas[i+1]){
                    isSorted = false;

                    buf = mas[i];
                    mas[i] = mas[i+1];
                    mas[i+1] = buf;
                }
            }
        }
        System.out.println("Элементы массива, увеличенные на 10% отсортированы по убыванию: " + Arrays.toString(mas));
    }
}