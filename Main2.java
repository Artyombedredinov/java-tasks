package ru.unn.lesson1;

import java.util.Scanner;

public class Main2 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите количество долларов в час (>=8)");
        float dol = in.nextFloat();

        System.out.println("Введите количество проработанных часов в неделю (<=60)");
        float hour = in.nextFloat();
        System.out.println(moneyPerWeek(dol, hour));
    }

    private static float moneyPerWeek(float dol, float hour) {

        byte q = 1;
        if (dol < 8 || hour > 60) {
            System.out.println("Условия не соблюдены");
        q = 0;}
        else {
            if (hour > 40) {
                float posle40 = hour - 40;
                hour = 40;
                posle40 *= 1.5;
                hour += posle40;
            }
            }
        return dol * hour * q;
    }
}
