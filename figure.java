package ru.unn.classes_and_objects;

public abstract class figure {
    public int x;
    public int y;
    public figure(int X, int Y){
        x=X;
        y=Y;
    }

    public abstract void square();

    public void getQuadrant(){
        if (x>0 & y>0) {
            System.out.println("Второй");
        }

        if (x<0 & y>0) {
            System.out.println("Первый");
        }

        if (x>0 & y<0) {
            System.out.println("Четвёртый");
        }

        if (x<0 & y<0) {
            System.out.println("Третий");
        }

        if (x==0 & y>0) {
            System.out.println("Между первым и вторым");
        }

        if (x==0 & y<0) {
            System.out.println("Между третим и четвёртым");
        }

        if (x>0 & y==0) {
            System.out.println("Между вторым и четвёртым");
        }

        if (x<0 & y==0) {
            System.out.println("Между первым и третим");
        }

        if (x==0 & y==0) {
            System.out.println("В начале координат");
        }

    }
}